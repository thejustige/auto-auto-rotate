package com.jarsilio.android.autoautorotate.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.jarsilio.android.autoautorotate.prefs.Prefs
import timber.log.Timber

class AutoStart : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val prefs = Prefs.getInstance(context)
        if (prefs.isEnabled) {
            if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
                Timber.d("Received ACTION_BOOT_COMPLETED.")
                PersistentService.startService(context)
            } else if (intent.action == Intent.ACTION_MY_PACKAGE_REPLACED) {
                Timber.d("Received ACTION_MY_PACKAGE_REPLACED.")
                PersistentService.startService(context)
            }
        }
    }
}
